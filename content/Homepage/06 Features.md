---
items:
  - title: Familiar
    desc: Scala 3 has a simple Python-like syntax, which can be learned quickly by Java programmers.

  - title: JVM
    desc: Scala 3 is powered by the JVM, the most proven platform for deploying server-side applications.

  - title: Ecosystem
    desc: Scala 3 interoperates with libraries and code in Java & Kotlin, and can benefit from JVM tooling.

  - title: Type-inference
    desc: Scala 3 has advanced type inference, retaining the ease of dynamic programming with the safety of static types.

  - title: Data Classes
    desc: Scala 3 has data classes and enums with rich features that eliminate boilerplate and code generation.

  - title: Generics
    desc: Scala 3 has a powerful generics system, allowing reuse of data structures and code across data types.

  - title: Functional
    desc: Scala 3 has a functional core, with first-class functions, immutable data, pattern matching, and type classes.

  - title: Code Derivation
    desc: Scala 3 has code derivation, a powerful feature that can derive serialization codecs and other code for you.

  - title: Meta programming
    desc: Scala 3 has advanced metaprogramming, allowing you to reduce boilerplate and improve performance.
---

## Features

Home is behind, the world ahead and there are many paths to tread through shadows to the edge.
