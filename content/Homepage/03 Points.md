---
features:
  - title: Productivity
    image: images/Tagline.svg
    text: Packed with features to make you <span>insanely productive</span>, like type-inference, data classes and code derivation.

  - title: Happiness
    image: images/Group.svg
    text: Makes building complex applications a joy, <span>no code duplication</span>, static types to guide you.

  - title: Reliability
    image: images/Shield-Done.svg
    text: Most powerful type system on the JVM. Write bulletproof applications that stay up and <span>don’t leak.</span>
---
