---
quote: Currently, Ford sells six sedans and coupes in North America with the Fiesta, Focus, Fusion, C-Max, Mustang and Taurus. This lineup hits.
items:
  - text: Currently, Ford sells six sedans and coupes in North America with the Fiesta, Focus, Fusion, C-Max, Mustang and Taurus. This lineup hits multiple segments from the compact Fiest to the mid-size Focus, C-Max and Fusion to the full-size Taurus.
    name: Emily Jørgensen
    desc: Author at Panoply Store
    image: images/testiclient.png
  - text: Currently, Ford sells six sedans and coupes in North America with the Fiesta, Focus, Fusion, C-Max, Mustang and Taurus. This lineup hits multiple segments from the compact Fiest to the mid-size Focus, C-Max and Fusion to the full-size Taurus.
    name: Emily Jørgensen
    desc: Author at Panoply Store
    image: images/testiclient.png
  - text: Currently, Ford sells six sedans and coupes in North America with the Fiesta, Focus, Fusion, C-Max, Mustang and Taurus. This lineup hits multiple segments from the compact Fiest to the mid-size Focus, C-Max and Fusion to the full-size Taurus.
    name: Emily Jørgensen
    desc: Author at Panoply Store
    image: images/testiclient.png
  - text: Currently, Ford sells six sedans and coupes in North America with the Fiesta, Focus, Fusion, C-Max, Mustang and Taurus. This lineup hits multiple segments from the compact Fiest to the mid-size Focus, C-Max and Fusion to the full-size Taurus.
    name: Emily Jørgensen
    desc: Author at Panoply Store
    image: images/testiclient.png
---

# Testimonials

Home is behind, the world ahead and there are many paths to tread through shadows to the edge.
