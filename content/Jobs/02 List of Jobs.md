---
items:
  - title: Senior Software Engineer
    company: Apple
    location: London, UK
    type: Full Time
    salary: $65k Starting
    desc: |
      Are you passionate about enterprise-wide scale compliance
      management? Are you excited about impactful technical
      projects that help our biggest enterprise customers manage
      hundreds of accounts with over a million resources across
      multiple regions? Amazon Web Services (AWS) is the pioneer
      and recognized leader in the Cloud. Our web services provide
      a platform for IT infrastructure that is used by hundreds of
      thousands of developers and businesses around the world.
    experience: |
      *   4+years of professional software development experience

      *   3+ years of programming experience with at least one modern language such as Java, C++, or C# including object-oriented design
          
      *   2+ years of experience contributing to the architecture and design (architecture, design patterns, reliability and scaling) of new and current systems
    what: |
      *   Assist users in navigating the intricacies of managing digital assets.

      *   Create and maintain support articles in our Knowledge Base to address common customer questions.
          
      *   Create above-and-beyond customer service experiences that surprise and delight.
          
      *   Use problem-solving abilities to resolve unique customer problems.
          
      *   Contribute actively to the team by providing candid and useful feedback to assist in building out the Customer Support team infrastructure{" "}
    offer: |
      *   The freedom to work wherever you are in the world.

      *   A chance to participate in building the future. Working in crypto gives you the opportunity to be a part of the next major technology wave in application{" "}
          
      *   technology and personal finance.
          
      *   A collaborative and feedback-driven culture where your voice matters

  - title: Senior Software Engineer
    company: Google
    location: London, UK
    type: Remote
    salary: $65k Starting
    desc: |
      Are you passionate about enterprise-wide scale compliance
      management? Are you excited about impactful technical
      projects that help our biggest enterprise customers manage
      hundreds of accounts with over a million resources across
      multiple regions? Amazon Web Services (AWS) is the pioneer
      and recognized leader in the Cloud. Our web services provide
      a platform for IT infrastructure that is used by hundreds of
      thousands of developers and businesses around the world.
    experience: |
      *   4+years of professional software development experience

      *   3+ years of programming experience with at least one modern language such as Java, C++, or C# including object-oriented design
          
      *   2+ years of experience contributing to the architecture and design (architecture, design patterns, reliability and scaling) of new and current systems
    what: |
      *   Assist users in navigating the intricacies of managing digital assets.

      *   Create and maintain support articles in our Knowledge Base to address common customer questions.
          
      *   Create above-and-beyond customer service experiences that surprise and delight.
          
      *   Use problem-solving abilities to resolve unique customer problems.
          
      *   Contribute actively to the team by providing candid and useful feedback to assist in building out the Customer Support team infrastructure{" "}
    offer: |
      *   The freedom to work wherever you are in the world.

      *   A chance to participate in building the future. Working in crypto gives you the opportunity to be a part of the next major technology wave in application{" "}
          
      *   technology and personal finance.
          
      *   A collaborative and feedback-driven culture where your voice matters

  - title: Senior Software Engineer
    company: Apple
    location: London, UK
    type: Full Time
    salary: $65k Starting
    desc: |
      Are you passionate about enterprise-wide scale compliance
      management? Are you excited about impactful technical
      projects that help our biggest enterprise customers manage
      hundreds of accounts with over a million resources across
      multiple regions? Amazon Web Services (AWS) is the pioneer
      and recognized leader in the Cloud. Our web services provide
      a platform for IT infrastructure that is used by hundreds of
      thousands of developers and businesses around the world.
    experience: |
      *   4+years of professional software development experience

      *   3+ years of programming experience with at least one modern language such as Java, C++, or C# including object-oriented design
          
      *   2+ years of experience contributing to the architecture and design (architecture, design patterns, reliability and scaling) of new and current systems
    what: |
      *   Assist users in navigating the intricacies of managing digital assets.

      *   Create and maintain support articles in our Knowledge Base to address common customer questions.
          
      *   Create above-and-beyond customer service experiences that surprise and delight.
          
      *   Use problem-solving abilities to resolve unique customer problems.
          
      *   Contribute actively to the team by providing candid and useful feedback to assist in building out the Customer Support team infrastructure{" "}
    offer: |
      *   The freedom to work wherever you are in the world.

      *   A chance to participate in building the future. Working in crypto gives you the opportunity to be a part of the next major technology wave in application{" "}
          
      *   technology and personal finance.
          
      *   A collaborative and feedback-driven culture where your voice matters

  - title: Senior Software Engineer
    company: Apple
    location: London, UK
    type: Full Time
    salary: $65k Starting
    desc: |
      Are you passionate about enterprise-wide scale compliance
      management? Are you excited about impactful technical
      projects that help our biggest enterprise customers manage
      hundreds of accounts with over a million resources across
      multiple regions? Amazon Web Services (AWS) is the pioneer
      and recognized leader in the Cloud. Our web services provide
      a platform for IT infrastructure that is used by hundreds of
      thousands of developers and businesses around the world.
    experience: |
      *   4+years of professional software development experience

      *   3+ years of programming experience with at least one modern language such as Java, C++, or C# including object-oriented design
          
      *   2+ years of experience contributing to the architecture and design (architecture, design patterns, reliability and scaling) of new and current systems
    what: |
      *   Assist users in navigating the intricacies of managing digital assets.

      *   Create and maintain support articles in our Knowledge Base to address common customer questions.
          
      *   Create above-and-beyond customer service experiences that surprise and delight.
          
      *   Use problem-solving abilities to resolve unique customer problems.
          
      *   Contribute actively to the team by providing candid and useful feedback to assist in building out the Customer Support team infrastructure{" "}
    offer: |
      *   The freedom to work wherever you are in the world.

      *   A chance to participate in building the future. Working in crypto gives you the opportunity to be a part of the next major technology wave in application{" "}
          
      *   technology and personal finance.
          
      *   A collaborative and feedback-driven culture where your voice matters

  - title: Senior Software Engineer
    company: Apple
    location: London, UK
    type: Full Time
    salary: $65k Starting
    desc: |
      Are you passionate about enterprise-wide scale compliance
      management? Are you excited about impactful technical
      projects that help our biggest enterprise customers manage
      hundreds of accounts with over a million resources across
      multiple regions? Amazon Web Services (AWS) is the pioneer
      and recognized leader in the Cloud. Our web services provide
      a platform for IT infrastructure that is used by hundreds of
      thousands of developers and businesses around the world.
    experience: |
      *   4+years of professional software development experience

      *   3+ years of programming experience with at least one modern language such as Java, C++, or C# including object-oriented design
          
      *   2+ years of experience contributing to the architecture and design (architecture, design patterns, reliability and scaling) of new and current systems
    what: |
      *   Assist users in navigating the intricacies of managing digital assets.

      *   Create and maintain support articles in our Knowledge Base to address common customer questions.
          
      *   Create above-and-beyond customer service experiences that surprise and delight.
          
      *   Use problem-solving abilities to resolve unique customer problems.
          
      *   Contribute actively to the team by providing candid and useful feedback to assist in building out the Customer Support team infrastructure{" "}
    offer: |
      *   The freedom to work wherever you are in the world.

      *   A chance to participate in building the future. Working in crypto gives you the opportunity to be a part of the next major technology wave in application{" "}
          
      *   technology and personal finance.
          
      *   A collaborative and feedback-driven culture where your voice matters

  - title: Senior Software Engineer
    company: Apple
    location: London, UK
    type: Full Time
    salary: $65k Starting
    desc: |
      Are you passionate about enterprise-wide scale compliance
      management? Are you excited about impactful technical
      projects that help our biggest enterprise customers manage
      hundreds of accounts with over a million resources across
      multiple regions? Amazon Web Services (AWS) is the pioneer
      and recognized leader in the Cloud. Our web services provide
      a platform for IT infrastructure that is used by hundreds of
      thousands of developers and businesses around the world.
    experience: |
      *   4+years of professional software development experience

      *   3+ years of programming experience with at least one modern language such as Java, C++, or C# including object-oriented design
          
      *   2+ years of experience contributing to the architecture and design (architecture, design patterns, reliability and scaling) of new and current systems
    what: |
      *   Assist users in navigating the intricacies of managing digital assets.

      *   Create and maintain support articles in our Knowledge Base to address common customer questions.
          
      *   Create above-and-beyond customer service experiences that surprise and delight.
          
      *   Use problem-solving abilities to resolve unique customer problems.
          
      *   Contribute actively to the team by providing candid and useful feedback to assist in building out the Customer Support team infrastructure{" "}
    offer: |
      *   The freedom to work wherever you are in the world.

      *   A chance to participate in building the future. Working in crypto gives you the opportunity to be a part of the next major technology wave in application{" "}
          
      *   technology and personal finance.
          
      *   A collaborative and feedback-driven culture where your voice matters

  - title: Senior Software Engineer
    company: Apple
    location: London, UK
    type: Full Time
    salary: $65k Starting
    desc: |
      Are you passionate about enterprise-wide scale compliance
      management? Are you excited about impactful technical
      projects that help our biggest enterprise customers manage
      hundreds of accounts with over a million resources across
      multiple regions? Amazon Web Services (AWS) is the pioneer
      and recognized leader in the Cloud. Our web services provide
      a platform for IT infrastructure that is used by hundreds of
      thousands of developers and businesses around the world.
    experience: |
      *   4+years of professional software development experience

      *   3+ years of programming experience with at least one modern language such as Java, C++, or C# including object-oriented design
          
      *   2+ years of experience contributing to the architecture and design (architecture, design patterns, reliability and scaling) of new and current systems
    what: |
      *   Assist users in navigating the intricacies of managing digital assets.

      *   Create and maintain support articles in our Knowledge Base to address common customer questions.
          
      *   Create above-and-beyond customer service experiences that surprise and delight.
          
      *   Use problem-solving abilities to resolve unique customer problems.
          
      *   Contribute actively to the team by providing candid and useful feedback to assist in building out the Customer Support team infrastructure{" "}
    offer: |
      *   The freedom to work wherever you are in the world.

      *   A chance to participate in building the future. Working in crypto gives you the opportunity to be a part of the next major technology wave in application{" "}
          
      *   technology and personal finance.
          
      *   A collaborative and feedback-driven culture where your voice matters
---
