---
contents:
  - title: Introduction
    page: 1
  - title: Lorem Ipsum
    page: 2
  - title: Consectetur
    page: 3
  - title: Adipiscing elit.
    page: 4
  - title: Id aliquam posuere
    page: 5
  - title: Netus augue
    page: 6
  - title: Dignissim ac morbi
    page: 7
  - title: Sagittis. Suscipit
    page: 8
  - title: Eleifend condimentum
    page: 9
  - title: Et vitae aliquam felis,
    page: 10
  - title: Nisl duis curabitur.
    page: 11

# Book Content
pages:
  - title: Introduction
    left: |

      Welcome to the Scala 3 Book. The goal of this book is to provide an informal introduction to the Scala language. It touches on all Scala topics, in a relatively light manner. If at any time while you’re reading this book and want more information on a specific feature, you’ll find links to our Reference documentation, which covers many new features of the Scala language in more detail.

      Over the course of this book, we hope to demonstrate that Scala is a beautiful, expressive programming language, with a clean, modern syntax, and supports functional programming (FP), object-oriented programming (OOP), and a fusion of FP and OOP in a typed setting. Scala’s syntax, grammar, and features have been re-thought, debated in an open process, and updated in 2020 to be more clear and easier to understand than ever before.

      Welcome to the Scala 3 Book. The goal of this book is to provide an informal introduction to the Scala language. It touches on all Scala topics, in a relatively light manner. If at any time while you’re reading this book and want more information on a specific feature, you’ll find links to our Reference documentation, which covers many new features of the Scala language in more detail.

      Over the course of this book, we hope to demonstrate that Scala is a beautiful, expressive programming language, with a clean, modern syntax, and supports functional programming (FP), object-oriented programming (OOP), and a fusion of FP and OOP in a typed setting. Scala’s syntax, grammar, and features have been re-thought, debated in an open process, and updated in 2020 to be more clear and easier to understand than ever before.
    right: |
      Welcome to the Scala 3 Book. The goal of this book is to provide an informal introduction to the Scala language. It touches on all Scala topics, in a relatively light manner. If at any time while you’re reading this book and want more information on a specific feature, you’ll find links to our Reference documentation, which covers many new features of the Scala language in more detail.

      The book begins with a whirlwind tour of many of Scala’s features in the “A Taste of Scala” section. After that tour, the sections that follow it provide more details on those language features.

      Welcome to the Scala 3 Book. The goal of this book is to provide an informal introduction to the Scala language. It touches on all Scala topics, in a relatively light manner. If at any time while you’re reading this book and want more information on a specific feature, you’ll find links to our Reference documentation, which covers many new features of the Scala language in more detail.

      The book begins with a whirlwind tour of many of Scala’s features in the “A Taste of Scala” section. After that tour, the sections that follow it provide more details on those language features.

  - title: Lorem Ipsum
    left: |
      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at euismod est. Phasellus quis urna felis. Quisque diam lorem, porttitor a mollis in, aliquam ac metus. Quisque gravida elit ut quam ullamcorper egestas. Pellentesque rutrum massa id rutrum blandit. Vivamus volutpat sodales dolor semper vehicula.

      Donec mattis enim mollis enim commodo rhoncus. Phasellus dui nisi, dignissim et magna in, porta venenatis ante. Etiam mollis, quam eget mattis pretium, nisl mi venenatis eros, et sagittis metus nulla quis enim. Vivamus mollis vulputate leo, a ornare nulla commodo nec. Maecenas in ultricies arcu, nec gravida mi. Aliquam suscipit velit eu elit lacinia laoreet at sit amet eros. Sed laoreet elementum tellus vel semper. Morbi ante mauris, elementum et tellus ac, semper mattis purus. Sed at quam a est sodales malesuada. Integer id turpis condimentum, tempor massa in, blandit augue. Duis dignissim enim bibendum laoreet fringilla.
    right: |
      Nunc sit amet arcu lacus. Morbi tristique quam lorem, eu condimentum dui tempus vitae. Integer commodo, justo ut mollis dapibus, neque sem blandit nulla, ac tristique augue risus eget odio. Pellentesque sem tellus, fringilla at mauris maximus, pharetra volutpat libero. Aenean leo urna, venenatis eu ipsum at, tempor lacinia eros. Cras dui erat, faucibus in odio nec, commodo rutrum arcu. In maximus tempor ultricies. Mauris eu augue metus. Donec volutpat lacus sit amet efficitur accumsan. Nullam auctor rutrum dui sit amet sagittis.

  - title: Consectetur
    left: |
      Nullam quis ex ac sem iaculis luctus quis eu est. Nunc ac leo sodales, bibendum elit non, pulvinar nunc. Integer dapibus tortor eget tincidunt tempor. Ut nec dignissim erat. Curabitur tempor sapien malesuada sapien sagittis cursus. Pellentesque eget nunc tempus, ultricies lorem quis, placerat tellus. Vivamus lobortis arcu non vulputate laoreet. Vivamus in ante ac ligula consequat bibendum. Pellentesque condimentum dolor felis, vel efficitur tortor semper eu. Pellentesque sed ligula est. Duis tristique eros sed enim ornare fringilla. Aliquam efficitur felis a vulputate rutrum. Nulla condimentum ac lectus eget ullamcorper.

      Praesent semper blandit odio at cursus. Nullam porta, nisl in interdum ultrices, leo ligula condimentum leo, quis hendrerit diam purus ut odio. Vivamus fringilla aliquet libero, at rutrum mi molestie et. Nam massa lacus, faucibus eget lacus vel, lobortis cursus eros. Suspendisse vel elit ornare nunc fermentum volutpat sit amet molestie elit. Etiam consequat, erat at tincidunt viverra, lorem orci faucibus lacus, eu auctor dolor neque nec sapien. Ut non purus eu ante pretium euismod quis vel sem. Sed fermentum commodo velit, ac imperdiet erat pharetra non. Ut ac lacus ex. In ac molestie leo, et varius augue. Aliquam erat volutpat. Ut semper efficitur mollis. Pellentesque sit amet nulla vitae lacus auctor dictum.
    right: |
      Pellentesque sem tortor, consequat nec nibh vitae, egestas euismod nisi. Nullam aliquet massa sem, nec lacinia velit porta dignissim. Sed quis massa ac magna sollicitudin aliquam a sed sem. Etiam sit amet eleifend arcu, at iaculis quam. Etiam pharetra auctor dapibus. Aliquam in hendrerit quam. Pellentesque sed risus eget augue vulputate elementum vitae at risus. Nam pretium vitae augue eu gravida. Nullam turpis ligula, tincidunt sit amet congue non, luctus a nibh. Etiam eget leo vitae purus lacinia gravida vitae id orci.

  - title: Adipiscing elit.
    left: |
      Aliquam erat volutpat. Quisque gravida nulla nec dui tempor ullamcorper. Vestibulum sagittis, est ultrices varius aliquet, nisi sapien venenatis enim, vitae sagittis lacus neque vel quam. Quisque at justo eget metus tincidunt vestibulum. Nam fermentum posuere massa eget pulvinar. Aliquam at ultricies arcu. Nulla lorem ante, mollis a suscipit feugiat, dapibus in urna. Nam vitae lacinia dui, ac mollis dolor. Quisque rutrum orci interdum tristique vestibulum.

      Suspendisse non viverra leo, eget tincidunt libero. Maecenas consequat elit orci, ac hendrerit magna vulputate id. In faucibus nisl quis sagittis imperdiet. Fusce tempor ipsum a mollis placerat. Quisque iaculis molestie auctor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc in arcu et sem accumsan venenatis. Cras vel leo at magna tincidunt convallis ac ac ligula. Donec velit purus, porttitor ut dolor fringilla, bibendum vulputate eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.
    right: |
      Pellentesque id tellus quis velit sollicitudin fringilla. Vivamus scelerisque, risus vitae sagittis pretium, quam nisl suscipit dolor, in gravida ante lectus sit amet augue. Fusce cursus feugiat nunc vitae ornare. Aliquam commodo velit tristique nibh vestibulum interdum. Mauris volutpat egestas semper. Praesent rutrum cursus pretium. Duis a erat lobortis, gravida eros at, porta odio.

  - title: Id aliquam posuere
    left: |
      Nam iaculis, risus sed aliquam suscipit, purus justo viverra tortor, non aliquam purus ligula mattis dolor. Vestibulum vitae sem mi. Sed fermentum lacus eget libero efficitur aliquam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris sollicitudin lorem non magna hendrerit dignissim. Cras tempus turpis id ante aliquet eleifend et vitae leo. Nam in vestibulum tellus. Duis ac lectus eros. Mauris blandit nunc in leo luctus hendrerit. In hac habitasse platea dictumst. Morbi ipsum elit, interdum et neque et, gravida congue tellus. Morbi euismod mi quis massa elementum ullamcorper. Sed nec lectus rutrum, laoreet est nec, porta massa. Donec consequat tincidunt turpis id lobortis. Ut vitae massa interdum, finibus ex sit amet, condimentum lacus. Morbi commodo tortor magna, sit amet dignissim neque dignissim quis.

      Cras porta purus vel risus venenatis, at elementum ipsum blandit. Maecenas cursus ultricies euismod. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec interdum sollicitudin nibh. Aliquam faucibus mauris eget quam hendrerit, et sollicitudin nisl maximus. Cras velit erat, posuere quis metus vitae, ultricies fermentum purus. Sed auctor facilisis maximus. Pellentesque posuere porta sem aliquam mollis. Aliquam vitae aliquam ante. Morbi consequat massa justo, in tincidunt tortor molestie vitae. Cras libero eros, rhoncus non nunc hendrerit, rutrum convallis arcu. Phasellus in lorem ligula. Maecenas varius massa vitae erat malesuada posuere. Sed scelerisque, leo eget laoreet semper, ipsum tortor bibendum sapien, sit amet ultricies risus mauris eget mi. Nulla vel massa sit amet eros dignissim pulvinar.
    right: |
      Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Mauris pretium diam eu velit pretium egestas. Sed vitae malesuada nulla. Vestibulum bibendum volutpat tortor, eget ultricies turpis interdum nec. Sed luctus lectus vitae libero eleifend pretium. Vivamus faucibus eros sit amet lobortis volutpat. Sed non massa venenatis, porttitor ex et, sodales odio. Maecenas et nibh diam. Vivamus posuere ex vitae mauris finibus venenatis. Vestibulum sagittis a odio a viverra. Sed nibh enim, pellentesque sed est ut, interdum facilisis augue. Nunc maximus facilisis blandit. Suspendisse odio mauris, tempus vel est id, malesuada sodales erat.

  - title: Netus augue
    left: |
      Welcome to the Scala 3 Book. The goal of this book is to provide an informal introduction to the Scala language. It touches on all Scala topics, in a relatively light manner. If at any time while you’re reading this book and want more information on a specific feature, you’ll find links to our Reference documentation, which covers many new features of the Scala language in more detail.

      Over the course of this book, we hope to demonstrate that Scala is a beautiful, expressive programming language, with a clean, modern syntax, and supports functional programming (FP), object-oriented programming (OOP), and a fusion of FP and OOP in a typed setting. Scala’s syntax, grammar, and features have been re-thought, debated in an open process, and updated in 2020 to be more clear and easier to understand than ever before.
    right: |
      The book begins with a whirlwind tour of many of Scala’s features in the “A Taste of Scala” section. After that tour, the sections that follow it provide more details on those language features.

  - title: Dignissim ac morbi
    left: |
      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at euismod est. Phasellus quis urna felis. Quisque diam lorem, porttitor a mollis in, aliquam ac metus. Quisque gravida elit ut quam ullamcorper egestas. Pellentesque rutrum massa id rutrum blandit. Vivamus volutpat sodales dolor semper vehicula.

      Donec mattis enim mollis enim commodo rhoncus. Phasellus dui nisi, dignissim et magna in, porta venenatis ante. Etiam mollis, quam eget mattis pretium, nisl mi venenatis eros, et sagittis metus nulla quis enim. Vivamus mollis vulputate leo, a ornare nulla commodo nec. Maecenas in ultricies arcu, nec gravida mi. Aliquam suscipit velit eu elit lacinia laoreet at sit amet eros. Sed laoreet elementum tellus vel semper. Morbi ante mauris, elementum et tellus ac, semper mattis purus. Sed at quam a est sodales malesuada. Integer id turpis condimentum, tempor massa in, blandit augue. Duis dignissim enim bibendum laoreet fringilla.
    right: |
      Nunc sit amet arcu lacus. Morbi tristique quam lorem, eu condimentum dui tempus vitae. Integer commodo, justo ut mollis dapibus, neque sem blandit nulla, ac tristique augue risus eget odio. Pellentesque sem tellus, fringilla at mauris maximus, pharetra volutpat libero. Aenean leo urna, venenatis eu ipsum at, tempor lacinia eros. Cras dui erat, faucibus in odio nec, commodo rutrum arcu. In maximus tempor ultricies. Mauris eu augue metus. Donec volutpat lacus sit amet efficitur accumsan. Nullam auctor rutrum dui sit amet sagittis.

  - title: Sagittis. Suscipit
    left: |
      Nullam quis ex ac sem iaculis luctus quis eu est. Nunc ac leo sodales, bibendum elit non, pulvinar nunc. Integer dapibus tortor eget tincidunt tempor. Ut nec dignissim erat. Curabitur tempor sapien malesuada sapien sagittis cursus. Pellentesque eget nunc tempus, ultricies lorem quis, placerat tellus. Vivamus lobortis arcu non vulputate laoreet. Vivamus in ante ac ligula consequat bibendum. Pellentesque condimentum dolor felis, vel efficitur tortor semper eu. Pellentesque sed ligula est. Duis tristique eros sed enim ornare fringilla. Aliquam efficitur felis a vulputate rutrum. Nulla condimentum ac lectus eget ullamcorper.

      Praesent semper blandit odio at cursus. Nullam porta, nisl in interdum ultrices, leo ligula condimentum leo, quis hendrerit diam purus ut odio. Vivamus fringilla aliquet libero, at rutrum mi molestie et. Nam massa lacus, faucibus eget lacus vel, lobortis cursus eros. Suspendisse vel elit ornare nunc fermentum volutpat sit amet molestie elit. Etiam consequat, erat at tincidunt viverra, lorem orci faucibus lacus, eu auctor dolor neque nec sapien. Ut non purus eu ante pretium euismod quis vel sem. Sed fermentum commodo velit, ac imperdiet erat pharetra non. Ut ac lacus ex. In ac molestie leo, et varius augue. Aliquam erat volutpat. Ut semper efficitur mollis. Pellentesque sit amet nulla vitae lacus auctor dictum.
    right: |
      Pellentesque sem tortor, consequat nec nibh vitae, egestas euismod nisi. Nullam aliquet massa sem, nec lacinia velit porta dignissim. Sed quis massa ac magna sollicitudin aliquam a sed sem. Etiam sit amet eleifend arcu, at iaculis quam. Etiam pharetra auctor dapibus. Aliquam in hendrerit quam. Pellentesque sed risus eget augue vulputate elementum vitae at risus. Nam pretium vitae augue eu gravida. Nullam turpis ligula, tincidunt sit amet congue non, luctus a nibh. Etiam eget leo vitae purus lacinia gravida vitae id orci.

  - title: Eleifend condimentum
    left: |
      Aliquam erat volutpat. Quisque gravida nulla nec dui tempor ullamcorper. Vestibulum sagittis, est ultrices varius aliquet, nisi sapien venenatis enim, vitae sagittis lacus neque vel quam. Quisque at justo eget metus tincidunt vestibulum. Nam fermentum posuere massa eget pulvinar. Aliquam at ultricies arcu. Nulla lorem ante, mollis a suscipit feugiat, dapibus in urna. Nam vitae lacinia dui, ac mollis dolor. Quisque rutrum orci interdum tristique vestibulum.

      Suspendisse non viverra leo, eget tincidunt libero. Maecenas consequat elit orci, ac hendrerit magna vulputate id. In faucibus nisl quis sagittis imperdiet. Fusce tempor ipsum a mollis placerat. Quisque iaculis molestie auctor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc in arcu et sem accumsan venenatis. Cras vel leo at magna tincidunt convallis ac ac ligula. Donec velit purus, porttitor ut dolor fringilla, bibendum vulputate eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.
    right: |
      Pellentesque id tellus quis velit sollicitudin fringilla. Vivamus scelerisque, risus vitae sagittis pretium, quam nisl suscipit dolor, in gravida ante lectus sit amet augue. Fusce cursus feugiat nunc vitae ornare. Aliquam commodo velit tristique nibh vestibulum interdum. Mauris volutpat egestas semper. Praesent rutrum cursus pretium. Duis a erat lobortis, gravida eros at, porta odio.

  - title: Et vitae aliquam felis,
    left: |
      Nam iaculis, risus sed aliquam suscipit, purus justo viverra tortor, non aliquam purus ligula mattis dolor. Vestibulum vitae sem mi. Sed fermentum lacus eget libero efficitur aliquam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris sollicitudin lorem non magna hendrerit dignissim. Cras tempus turpis id ante aliquet eleifend et vitae leo. Nam in vestibulum tellus. Duis ac lectus eros. Mauris blandit nunc in leo luctus hendrerit. In hac habitasse platea dictumst. Morbi ipsum elit, interdum et neque et, gravida congue tellus. Morbi euismod mi quis massa elementum ullamcorper. Sed nec lectus rutrum, laoreet est nec, porta massa. Donec consequat tincidunt turpis id lobortis. Ut vitae massa interdum, finibus ex sit amet, condimentum lacus. Morbi commodo tortor magna, sit amet dignissim neque dignissim quis.

      Cras porta purus vel risus venenatis, at elementum ipsum blandit. Maecenas cursus ultricies euismod. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec interdum sollicitudin nibh. Aliquam faucibus mauris eget quam hendrerit, et sollicitudin nisl maximus. Cras velit erat, posuere quis metus vitae, ultricies fermentum purus. Sed auctor facilisis maximus. Pellentesque posuere porta sem aliquam mollis. Aliquam vitae aliquam ante. Morbi consequat massa justo, in tincidunt tortor molestie vitae. Cras libero eros, rhoncus non nunc hendrerit, rutrum convallis arcu. Phasellus in lorem ligula. Maecenas varius massa vitae erat malesuada posuere. Sed scelerisque, leo eget laoreet semper, ipsum tortor bibendum sapien, sit amet ultricies risus mauris eget mi. Nulla vel massa sit amet eros dignissim pulvinar.
    right: |
      Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Mauris pretium diam eu velit pretium egestas. Sed vitae malesuada nulla. Vestibulum bibendum volutpat tortor, eget ultricies turpis interdum nec. Sed luctus lectus vitae libero eleifend pretium. Vivamus faucibus eros sit amet lobortis volutpat. Sed non massa venenatis, porttitor ex et, sodales odio. Maecenas et nibh diam. Vivamus posuere ex vitae mauris finibus venenatis. Vestibulum sagittis a odio a viverra. Sed nibh enim, pellentesque sed est ut, interdum facilisis augue. Nunc maximus facilisis blandit. Suspendisse odio mauris, tempus vel est id, malesuada sodales erat.

  - title: Nisl duis curabitur.
    left: |
      Welcome to the Scala 3 Book. The goal of this book is to provide an informal introduction to the Scala language. It touches on all Scala topics, in a relatively light manner. If at any time while you’re reading this book and want more information on a specific feature, you’ll find links to our Reference documentation, which covers many new features of the Scala language in more detail.

      Over the course of this book, we hope to demonstrate that Scala is a beautiful, expressive programming language, with a clean, modern syntax, and supports functional programming (FP), object-oriented programming (OOP), and a fusion of FP and OOP in a typed setting. Scala’s syntax, grammar, and features have been re-thought, debated in an open process, and updated in 2020 to be more clear and easier to understand than ever before.
    right: |
      The book begins with a whirlwind tour of many of Scala’s features in the “A Taste of Scala” section. After that tour, the sections that follow it provide more details on those language features.
---
