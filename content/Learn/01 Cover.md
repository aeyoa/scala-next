---
button: Buy Book
url: /learn
sponsored: |
  ### This free material is generously sponsored by **Ziverge Inc.**
---

# Learn

With our high-quality online curriculum, developed by John A.
De Goes, everyone can master Scala 3.
