---
features:
  - title: Feature Title
    desc: It's a dangerous business, Frodo, going out your door.
    icon: images/feature_1.svg
  - title: Feature Title
    desc: It's a dangerous business, Frodo, going out your door.
    icon: images/feature_2.svg
  - title: Feature Title
    desc: It's a dangerous business, Frodo, going out your door.
    icon: images/feature_3.svg
  - title: Feature Title
    desc: It's a dangerous business, Frodo, going out your door.
    icon: images/feature_4.svg
  - title: Feature Title
    desc: It's a dangerous business, Frodo, going out your door.
    icon: images/feature_5.svg
  - title: Feature Title
    desc: It's a dangerous business, Frodo, going out your door.
    icon: images/feature_6.svg
---

# Features

Home is behind, the world ahead and there are many paths to tread through shadows to the edge, world ahead and there are.
