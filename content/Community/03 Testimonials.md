---
items:
  - name: John Doe
    desc: Author at Panoply Store
    text: “I discovered a whole community of Scala 3 experts.”
  - name: Mi Yong
    desc: Author at Panoply Store
    text: “Amazing content from some of the best minds in Scala.”
  - name: Mike Oldfield
    desc: Author at Panoply Store
    text: “I found my current job from networking on Scala3.com.”
  - name: John Doe
    desc: Author at Panoply Store
    text: “I discovered a whole community of Scala 3 experts.”
---
