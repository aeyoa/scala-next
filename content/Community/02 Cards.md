---
cards:
  - heading: Learn
    image: images/girl copy.png
    desc: Home is behind, the world ahead and there are many paths to tread through.
    icon: images/Vector.svg
  - heading: Share
    image: images/city.png
    desc: Home is behind, the world ahead and there are many paths to tread through.
    icon: images/outline.svg
  - heading: Network
    image: images/light.png
    desc: Home is behind, the world ahead and there are many paths to tread through.
    icon: images/Vector (1).svg
---
