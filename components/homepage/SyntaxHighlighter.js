import { Light as LightHighlighter } from "react-syntax-highlighter"

const scala = function (hljs) {
  const ANNOTATION = {
    className: "meta",
    begin: "@[A-Za-z]+",
  }

  // used in strings for escaping/interpolation/substitution
  const SUBST = {
    className: "subst",
    variants: [
      {
        begin: "\\$[A-Za-z0-9_]+",
      },
      {
        begin: /\$\{/,
        end: /\}/,
      },
    ],
  }

  const STRING = {
    className: "string",
    variants: [
      {
        begin: '"""',
        end: '"""',
      },
      {
        begin: '"',
        end: '"',
        illegal: "\\n",
        contains: [hljs.BACKSLASH_ESCAPE],
      },
      {
        begin: '[a-z]+"',
        end: '"',
        illegal: "\\n",
        contains: [hljs.BACKSLASH_ESCAPE, SUBST],
      },
      {
        className: "string",
        begin: '[a-z]+"""',
        end: '"""',
        contains: [SUBST],
        relevance: 10,
      },
    ],
  }

  const SYMBOL = {
    className: "symbol",
    begin: "'\\w[\\w\\d_]*(?!')",
  }

  const TYPE = {
    className: "type",
    begin: "\\b[A-Z][A-Za-z0-9_]*",
    relevance: 0,
  }

  const NAME = {
    className: "title",
    begin: /[^0-9\n\t "'(),.`{}\[\]:;][^\n\t "'(),.`{}\[\]:;]+|[^0-9\n\t "'(),.`{}\[\]:;=]/,
    relevance: 0,
  }

  const CLASS = {
    className: "class",
    beginKeywords: "class object trait type",
    end: /[:={\[\n;]/,
    excludeEnd: true,
    contains: [
      hljs.C_LINE_COMMENT_MODE,
      hljs.C_BLOCK_COMMENT_MODE,
      {
        beginKeywords: "extends with",
        relevance: 10,
      },
      {
        begin: /\[/,
        end: /\]/,
        excludeBegin: true,
        excludeEnd: true,
        relevance: 0,
        contains: [TYPE],
      },
      {
        className: "params",
        begin: /\(/,
        end: /\)/,
        excludeBegin: true,
        excludeEnd: true,
        relevance: 0,
        contains: [TYPE],
      },
      NAME,
    ],
  }

  const METHOD = {
    className: "function",
    beginKeywords: "def",
    end: /[:={\[(\n;]/,
    excludeEnd: true,
    contains: [NAME],
  }

  return {
    name: "Scala",
    keywords: {
      literal: "true false null",
      keyword:
        "type yield lazy override def with val var sealed abstract private trait object if forSome for while throw finally protected extends import final return else break new catch super class case package default try this match continue throws implicit",
    },
    contains: [
      hljs.C_LINE_COMMENT_MODE,
      hljs.C_BLOCK_COMMENT_MODE,
      STRING,
      SYMBOL,
      TYPE,
      METHOD,
      CLASS,
      hljs.C_NUMBER_MODE,
      ANNOTATION,
    ],
  }
}

const anOldHope = {
  "hljs-comment": {
    color: "#B6B18B",
  },
  "hljs-quote": {
    color: "#B6B18B",
  },
  "hljs-variable": {
    color: "#EB3C54",
  },
  "hljs-template-variable": {
    color: "#EB3C54",
  },
  "hljs-tag": {
    color: "#EB3C54",
  },
  "hljs-name": {
    color: "#EB3C54",
  },
  "hljs-selector-id": {
    color: "#EB3C54",
  },
  "hljs-selector-class": {
    color: "#EB3C54",
  },
  "hljs-regexp": {
    color: "#EB3C54",
  },
  "hljs-deletion": {
    color: "#EB3C54",
  },
  "hljs-number": {
    color: "#FFC600",
  },
  "hljs-built_in": {
    color: "#FFC600",
  },
  "hljs-builtin-name": {
    color: "#FFC600",
  },
  "hljs-literal": {
    color: "#FFC600",
  },
  "hljs-type": {
    color: "#FFC600",
  },
  "hljs-params": {
    color: "#FFC600",
  },
  "hljs-meta": {
    color: "#FFC600",
  },
  "hljs-link": {
    color: "#FFC600",
  },
  "hljs-attribute": {
    color: "#EE7C2B",
  },
  "hljs-string": {
    color: "#4EC8FF",
  },
  "hljs-symbol": {
    color: "#4EC8FF",
  },
  "hljs-bullet": {
    color: "#4EC8FF",
  },
  "hljs-addition": {
    color: "#4EC8FF",
  },
  "hljs-title": {
    color: "#6ce44a",
  },
  "hljs-section": {
    color: "#6ce44a",
  },
  "hljs-keyword": {
    color: "#FF5CB7",
  },
  "hljs-selector-tag": {
    color: "#FF5CB7",
  },
  hljs: {
    display: "block",
    overflowX: "auto",
    background: "#1C1D21",
    color: "#c0c5ce",
    padding: "0.5em",
  },
  "hljs-emphasis": {
    fontStyle: "italic",
  },
  "hljs-strong": {
    fontWeight: "bold",
  },
}

LightHighlighter.registerLanguage("scala", scala)

const SyntaxHighligher = (props) => {
  return (
    <LightHighlighter
      language="scala"
      style={anOldHope}
      customStyle={{
        background: "none",
        fontFamily: "JetBrains Mono",
      }}>
      {props.children}
    </LightHighlighter>
  )
}

export default SyntaxHighligher
