/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../../lib/breakpoints"
import content from "../../content/Homepage/06 Features.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()

const html = {
  __html: md.render(matter(content).content),
}

const items = matter(content).data.items.map((el, index) => (
  <div
    css={css(
      mq({
        flexBasis: ["50%", "50%", "33.33%"],
      })
    )}
    key={index}
    className="col-6 col-sm-6 col-md-4 feature-box"
    data-number={index < 9 ? `0${index + 1}` : `${index + 1}`}>
    <h3 id="familiar">{el.title}</h3>
    <p>{el.desc}</p>
  </div>
))

const Features = () => {
  return (
    <div className="features">
      <div className="features-animate">
        <div className="container-lg">
          <div className="parallax-element feature-logo">
            <img src="images/feature-logo.png" />
          </div>
          <div className="row">
            <div className="col-md-4">
              <div
                className="heading-area"
                dangerouslySetInnerHTML={html}></div>
            </div>
          </div>
        </div>
        <div
          css={css(
            mq({
              display: "flex",
              flexWrap: "wrap",
              padding: [0, "0 30px", 0],
            })
          )}
          className="container-lg feature-boxes-container">
          {items}
        </div>
      </div>
      <div className="parallax-element top left net4 net-hide">
        <img src="images/Net 4.png" />
      </div>
      <div className="dividerLine dividerLine--right container-lg">
        <div className="row">
          <div className="col">
            <div className="dividerLine-underline" />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="dividerLine-highlight dividerLine-highlight--left" />
          </div>
          <div className="col">
            <div className="dividerLine-highlight dividerLine-highlight--center" />
          </div>
          <div className="col">
            <div className="dividerLine-highlight dividerLine-highlight--right" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Features
