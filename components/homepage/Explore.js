/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../../lib/breakpoints"
import content from "../../content/Homepage/07 Explore.md"
import matter from "gray-matter"
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
SwiperCore.use([Pagination])

const data = matter(content).data

const items = data.items.map((item, index) => {
  return (
    <SwiperSlide key={index}>
      <div
        css={css(mq({ paddingBottom: [70, 70, 0] }))}
        className="explore-box">
        <p>
          <img src={item.image} />
        </p>
        <h4 className="learn-scala-3">{item.title}</h4>
        <p>{item.desc}</p>
        <p>
          <a href={item.url}>{item.link}</a>
          <img css={css(mq({ marginLeft: 7 }))} src="images/next-icon.svg" />
        </p>
      </div>
    </SwiperSlide>
  )
})

const Explore = () => {
  return (
    <div style={{ position: "relative", zIndex: 10 }} className="explore">
      <div style={{ zIndex: 2 }} className="container-lg explore-container">
        <div className="d-flex justify-content-center text-center heading-area-section">
          <h2>{data.heading}</h2>
        </div>
        <div
          className="swiper-container use-case-slider explore-slider"
          data-options="explore-slider">
          <Swiper
            pagination={true}
            slidesPerView={1}
            spaceBetween={30}
            watchOverflow={true}
            breakpoints={{
              576: {
                slidesPerView: 2,
                spaceBetween: 32,
              },
              768: {
                slidesPerView: 4,
                spaceBetween: 46,
              },
            }}>
            {items}
          </Swiper>
        </div>
      </div>
      <div className="element-bg explore-sky">
        <img src="images/Bottom-BG-GROUP_2.jpg" />
      </div>
      <div className="element-bg explore-sky-radial">
        <img src="images/explore-bg-radial.png" />
      </div>
      <div className="element-bg explore-sky-parallax">
        <img src="images/explore-section-parallax.jpg" />
      </div>
    </div>
  )
}

export default Explore
