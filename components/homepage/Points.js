import matter from "gray-matter"
import content from "../../content/Homepage/03 Points.md"

const Points = () => {
  const items = matter(content).data.features.map((el, index) => {
    return (
      <div
        key={index}
        className={`points-boxes points-boxes--${index} col-md-4`}>
        <h3>
          <img src={el.image} />
          {el.title}
        </h3>
        <p dangerouslySetInnerHTML={{ __html: el.text }}></p>
      </div>
    )
  })
  return (
    <div className="points">
      <div className="container-lg">
        <div className="row">{items}</div>
      </div>
    </div>
  )
}

export default Points
