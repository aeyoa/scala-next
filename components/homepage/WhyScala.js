/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../../lib/breakpoints"

import Content from "../../content/Homepage/02 Why Scala.mdx"
import SyntaxHighlighter from "./SyntaxHighlighter"
import Divider from "../Divider"

const code = `
def fib(n: Int): Int = 
  if (n ≤  1) n 
  else fib(n - 1) + fib(n - 2)

@main 
  def example = 
  println(s"fib(10) = \${fib(10)}}")
`

const WhyScala = () => {
  return (
    <div className="whyscala" css={css(mq({ paddingTop: 0 }))}>
      <div className="container-lg">
        <div className="row">
          <div className="col-xl-5">
            <div className="whyscala-cont">
              <Content />
            </div>
          </div>
          <div className="col-xl-6 ml-auto p-2 whyscala-code">
            <SyntaxHighlighter>{code}</SyntaxHighlighter>
          </div>
        </div>
      </div>
      <Divider />
    </div>
  )
}

export default WhyScala
