/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../../lib/breakpoints"

import Divider from "../Divider"
import content from "../../content/About/05 Features.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
import { useState } from "react"
SwiperCore.use([Pagination])

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

const Feature = props => (
  <div style={{ position: "relative" }} className="d-flex">
    <div css={css(mq({ paddingBottom: [50, 50, 0] }))} className="feature-cont">
      <img src={props.icon} />
      <h5 className="feature-title">{props.title}</h5>
      <p className="feature-desc">{props.desc}</p>
    </div>
  </div>
)

const slides = data.features.map((el, i) => (
  <SwiperSlide key={i}>
    <Feature {...el} />
  </SwiperSlide>
))

const items = data.features.map((el, i) => (
  <div key={i} className="col-lg-4 col-md-4 mb-5">
    <Feature {...el} />
  </div>
))

const Features = () => {
  return (
    <div className="features">
      <Divider center></Divider>
      <div dangerouslySetInnerHTML={html} className="heading text-center"></div>
      <div
        className="swiper-container features-slider"
        data-options="features-slider">
        <div className="swiper-wrapper">
          <Swiper pagination>{slides}</Swiper>
        </div>
      </div>
      <div className="row features-row">{items}</div>
    </div>
  )
}

export default Features
