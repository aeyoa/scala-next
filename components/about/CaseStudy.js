import content from "../../content/About/04 Case Study.md"
import matter from "gray-matter"
const data = matter(content).data

const CaseStudy = () => {
  return (
    <div className="case-study">
      <div className="container">
        <div className="parallax-element about-p-1 top left">
          <img src="images/About-Middle-Paralax-1.png" />
        </div>
        <div className="parallax-element about-p-2 bottom right">
          <img src="images/About-Middle-Paralax-2.png" />
        </div>
        <div className="text-right position-relative">
          <div className="text text-left case-study-text">
            <p className="small-text">{data.category}</p>
            <h1>{data.title}</h1>
            <div className="text-right">
              <a href={data.link} className="button-3">
                {data.button}
              </a>
            </div>
          </div>
          <img
            className="ms-auto me-0 case-study-image"
            src="images/jIBMSMs4_kA.png"
          />
        </div>
        <div className="desc">
          <h3>{data.subtitle}</h3>
          <div className="row">
            <div className="col-lg-6 col-md-6">
              <p>{data.left}</p>
            </div>
            <div className="col-lg-6 col-md-6 responsive-desc">
              <p>{data.right}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CaseStudy
