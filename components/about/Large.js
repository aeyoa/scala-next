import content from "../../content/About/02 Large.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

const Large = () => {
  return (
    <div className="row row-top--880">
      <div className="col-lg-6 col-md-6 mt-auto order-1  order-md-0">
        <div className="about-heading" dangerouslySetInnerHTML={html}></div>
      </div>
      <div className="col-lg-6 col-md-6 mt-5 responsive-img">
        <img className="w-100 about-element" src={data.image} />
      </div>
    </div>
  )
}

export default Large
