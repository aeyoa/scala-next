import { Remarkable } from "remarkable"
const md = new Remarkable()
import { useState } from "react"

const JobCard = ({ data }) => {
  const [open, setOpen] = useState(false)
  return (
    <div className="job-card">
      <div className="job-card-min">
        <div className="d-flex content">
          <h5>{data.title}</h5>
          <hr className="seprator" />
          <div className="location">
            <p className="card-inner-head">Company</p>
            <p className="card-inner-title">{data.company}</p>
          </div>
          <div className="location">
            <p className="card-inner-head">Location</p>
            <p className="card-inner-title">{data.location}</p>
          </div>
          <div className="location">
            <p className="card-inner-head">Type</p>
            <p className="card-inner-title">{data.type}</p>
          </div>
          <div className="d-flex ml-auto">
            <div
              style={{
                cursor: "pointer",
                userSelect: "none",
              }}
              onClick={_ => {
                setOpen(!open)
              }}
              className={`open-detail button-3 d-2 ${open ? "active" : null}`}>
              Details{" "}
              <svg
                width={20}
                height={35}
                viewBox="0 0 20 35"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M10 1.96457L13 0H7L10 1.96457Z"
                  fill="#9DAFBD"
                />
              </svg>
            </div>
          </div>
        </div>
      </div>
      <div className="full-card" style={{ display: open ? "block" : "none" }}>
        <div>
          <div className="row">
            <div className="col-lg-4 col-md-4">
              <div className="full-card-content">
                <h3 className="job-title">{data.title}</h3>
                <hr />
                <div>
                  <p className="card-inner-head">Company</p>
                  <p className="card-inner-title">{data.company}</p>
                </div>
                <hr />
                <div>
                  <p className="card-inner-head">Location</p>
                  <p className="card-inner-title">{data.location}</p>
                </div>
                <hr />
                <div>
                  <p className="card-inner-head">Type</p>
                  <p className="card-inner-title">{data.type}</p>
                </div>
                <hr />
                <div>
                  <p className="card-inner-head">Salary</p>
                  <p className="card-inner-title">{data.salary}</p>
                </div>
              </div>
            </div>
            <div className="col-lg-8 col-md-8">
              <div className="job-detail">
                <div className="description">
                  <h5>Description</h5>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: md.render(data.desc),
                    }}></div>
                </div>
                <div className="exp">
                  <h5>Experience</h5>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: md.render(data.experience),
                    }}></div>
                </div>
                <div className="exp">
                  <h5>What You Will Do</h5>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: md.render(data.what),
                    }}></div>
                </div>
                <div className="exp">
                  <h5>What We Offer</h5>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: md.render(data.offer),
                    }}></div>
                </div>
                <div className="apply-btn exp">
                  <button className="button-3">Apply Now</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default JobCard
