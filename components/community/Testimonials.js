import content from "../../content/Community/03 Testimonials.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
import { useState } from "react"
SwiperCore.use([Pagination])

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

let swiperInstance

const slides = data.items.map((el, i) => (
  <SwiperSlide key={i}>
    <div style={{ marginBottom: 0 }} className="single-testimonial text-center">
      <div className="single-testimonial-border">
        <svg
          width={49}
          height={49}
          viewBox="0 0 49 49"
          fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            opacity="0.7"
            d="M15.6827 22.5845C14.4758 22.237 13.269 22.0607 12.095 22.0607C10.2822 22.0607 8.76952 22.475 7.59659 22.9823C8.72737 18.8426 11.4438 11.6996 16.8551 10.8952C17.3562 10.8207 17.7669 10.4583 17.9036 9.97054L19.0863 5.74038C19.186 5.38264 19.1269 4.99972 18.9228 4.68926C18.7188 4.37881 18.3909 4.17167 18.0234 4.1213C17.624 4.06682 17.2169 4.03906 16.8134 4.03906C10.3182 4.03906 3.8856 10.8186 1.17123 20.5259C-0.422128 26.221 -0.889343 34.7831 3.03547 40.1723C5.23174 43.1879 8.43594 44.7982 12.5591 44.9591C12.5761 44.9596 12.5926 44.9601 12.6095 44.9601C17.697 44.9601 22.2082 41.5338 23.5806 36.6288C24.4004 33.6965 24.0298 30.6213 22.5362 27.9675C21.0584 25.3436 18.6247 23.431 15.6827 22.5845Z"
            fill="white"
          />
          <path
            opacity="0.4"
            d="M38.5357 27.968C37.058 25.3436 34.6243 23.431 31.6822 22.5845C30.4753 22.237 29.2685 22.0607 28.0951 22.0607C26.2822 22.0607 24.7691 22.475 23.5961 22.9823C24.7269 18.8426 27.4433 11.6996 32.8551 10.8952C33.3562 10.8207 33.7664 10.4583 33.9036 9.97054L35.0863 5.74038C35.186 5.38264 35.1269 4.99972 34.9229 4.68926C34.7193 4.37881 34.3914 4.17167 34.0234 4.1213C33.6245 4.06682 33.2175 4.03906 32.8135 4.03906C26.3182 4.03906 19.8857 10.8186 17.1708 20.5259C15.5779 26.221 15.1107 34.7831 19.036 40.1733C21.2318 43.1884 24.4365 44.7992 28.5592 44.9596C28.5762 44.9601 28.5926 44.9606 28.6101 44.9606C33.697 44.9606 38.2088 41.5344 39.5812 36.6293C40.3999 33.697 40.0288 30.6212 38.5357 27.968Z"
            fill="white"
          />
        </svg>
        <p className="num">{`0${i + 1}`}</p>
        <p className="desc">{el.text}</p>
        <h5 className="name">{el.name}</h5>
        <span className="author">{el.desc}</span>
      </div>
    </div>
  </SwiperSlide>
))

const Testimonials = () => {
  const [state, setState] = useState("isBeginning")
  return (
    <div className="testimonials-area">
      <div className="container">
        <div className="testimonials">
          <div
            className="swiper-container testimonials"
            data-options="testimonials">
            <Swiper
              onSwiper={swiper => (swiperInstance = swiper)}
              onSlideChange={() => {
                if (swiperInstance.isBeginning) {
                  setState("isBeginning")
                  return
                }
                if (swiperInstance.isEnd) {
                  setState("isEnd")
                  return
                }
                setState("justSlidin")
              }}
              breakpoints={{
                768: {
                  slidesPerView: 3,
                  // spaceBetween: 48,
                },
              }}
              pagination={true}
              slidesPerView={1}
              spaceBetween={0}>
              {slides}
            </Swiper>
            <div
              style={{ marginTop: -45, zIndex: 9, position: "relative" }}
              className="d-none d-md-flex swiper-controls-group">
              <div className="swiper-pagination" />
              <div
                style={{ display: "flex" }}
                className="swiper-controls button-group">
                <div
                  onClick={() => swiperInstance.slidePrev()}
                  className={`${
                    state != "isBeginning" ? "active" : null
                  } slider-btn swiper-button-prev`}>
                  <svg
                    width={8}
                    height={14}
                    viewBox="0 0 8 14"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M0.287829 6.30465L6.3223 0.287096C6.70617 -0.095891 7.32854 -0.095891 7.71222 0.287096C8.09593 0.669742 8.09593 1.29036 7.71222 1.67297L2.37264 6.99759L7.71206 12.322C8.09577 12.7048 8.09577 13.3254 7.71206 13.708C7.32835 14.0908 6.70601 14.0908 6.32215 13.708L0.287674 7.69037C0.095819 7.49895 0 7.24835 0 6.99762C0 6.74676 0.096005 6.49597 0.287829 6.30465Z"
                      fill="#9DAFBD"
                    />
                  </svg>
                </div>
                <div
                  onClick={() => swiperInstance.slideNext()}
                  className={`${
                    state != "isEnd" ? "active" : null
                  } slider-btn swiper-button-next`}>
                  <svg
                    width={8}
                    height={14}
                    viewBox="0 0 8 14"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M7.71217 7.69047L1.6777 13.708C1.29383 14.091 0.671461 14.091 0.287783 13.708C-0.0959275 13.3254 -0.0959275 12.7048 0.287783 12.3221L5.62736 6.99753L0.287938 1.6731C-0.0957722 1.2903 -0.0957722 0.669747 0.287938 0.287101C0.671648 -0.0957002 1.29399 -0.0957002 1.67785 0.287101L7.71233 6.30475C7.90418 6.49616 8 6.74677 8 6.9975C8 7.24835 7.904 7.49915 7.71217 7.69047Z"
                      fill="white"
                    />
                  </svg>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Testimonials
