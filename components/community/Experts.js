import content from "../../content/Community/04 Experts.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
import { useState } from "react"
SwiperCore.use([Pagination])

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

const slides = data.experts.map((el, i) => (
  <SwiperSlide key={i}>
    <div style={{ paddingBottom: 60 }} className="text-center expert">
      <img src={el.image} />
      <h5 className="expert-name">{el.name}</h5>
      <p className="expert-post">{el.desc}</p>
    </div>
  </SwiperSlide>
))

const Experts = () => {
  return (
    <div style={{ position: "relative", zIndex: 5 }} className="container">
      <div className="experts">
        <div className="parallax-element community-three-pimg">
          <img src="images/Community_Line_3.png" />
        </div>
        <div className="parallax-element center-diamond">
          <svg
            width={1338}
            height={1337}
            viewBox="0 0 1338 1337"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <rect
              opacity="0.04"
              x="0.681641"
              y="668.439"
              width="945.316"
              height="945.316"
              transform="rotate(-45 0.681641 668.439)"
              fill="#3C4CDC"
            />
          </svg>
        </div>
        <div style={{ paddingBottom: 100 }} className="experts_animated-area">
          <div dangerouslySetInnerHTML={html} className="expert-heading"></div>
          <div
            className="swiper-container community-card-slider"
            data-options="community-card-slider">
            <Swiper
              breakpoints={{
                768: {
                  slidesPerView: 3,
                  spaceBetween: 18,
                },
              }}
              watchOverflow
              pagination={true}
              slidesPerView={1}
              spaceBetween={0}>
              {slides}
            </Swiper>
          </div>
          <div className="join-btn text-center">
            <a href={data.link} className="button-3">
              {data.button}
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Experts
