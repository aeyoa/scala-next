/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../lib/breakpoints"

const BackgroundImage = props => {
  return (
    <div
      css={css(
        mq({
          position: "absolute",
        })
      )}>
      <img
        src={props.src}
        css={css(
          mq({
            position: "absolute",
          })
        )}
      />
    </div>
  )
}

export default BackgroundImage
