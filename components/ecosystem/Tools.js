import content from "../../content/Ecosystem/03 Tools.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
import { useState } from "react"
SwiperCore.use([Pagination])

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

let swiperInstance

const Tools = () => {
  const [state, setState] = useState("isBeginning")
  const [index, setIndex] = useState(0)

  const slides = data.tools.map((el, i) => (
    <SwiperSlide key={i}>
      <div
        style={{ marginBottom: 70 }}
        className="text-area text-left"
        onClick={_ => setIndex(i)}>
        <h4 className="tool-name">{el.title}</h4>
        <div className="d-flex about-tool">
          <div className="num">
            <p>{`${i < 9 ? "0" : ""}${i + 1}`}</p>
          </div>
          <p className="text">{el.desc}</p>
        </div>
        <a>{el.button}</a>
      </div>
    </SwiperSlide>
  ))

  return (
    <div className="tool">
      <div
        className="parallax-element tool-circle parallax-animate"
        data-class=".tool-circle"
        data-start={0}
        data-end={5}
        data-scroll=".tool">
        <img src="images/tools-bg.png" />
      </div>
      <div className="tools-upper-half">
        <div
          dangerouslySetInnerHTML={html}
          className="tools-heading parallax-animate"
          data-class=".tools-heading"
          data-start={0}
          data-end={-65}
          data-scroll=".tool"></div>
        <div className="row">
          <div className="col-lg-6 col-md-6 order-1  order-md-0">
            <div
              className="text-area parallax-animate--play"
              data-class=".text-area.parallax-animate"
              data-start={0}
              data-end={-65}
              data-scroll=".tool">
              <h1 data-tool-name>{data.tools[index].title}</h1>
              <p data-tool-description>{data.tools[index].details}</p>
            </div>
          </div>
          <div className="col-lg-6 col-md-6">
            <a
              target="_blank"
              href={data.tools[index].link}
              className="learn-btn parallax-animate"
              data-class=".learn-btn"
              data-start={0}
              data-end={5}
              data-scroll=".tool">
              <p>{data.tools[index].button}</p>
            </a>
          </div>
        </div>
      </div>
      <div
        className="tools-bottom-half  parallax-animate"
        data-class=".tools-bottom-half"
        data-start={0}
        data-end={-15}
        data-scroll=".tools-bottom-half">
        {/* Swiper */}
        <div className="container">
          <div
            className="swiper-container toolsSlider"
            data-options="toolsSlider">
            <Swiper
              onSwiper={swiper => (swiperInstance = swiper)}
              onSlideChange={() => {
                if (swiperInstance.isBeginning) {
                  setState("isBeginning")
                  return
                }
                if (swiperInstance.isEnd) {
                  setState("isEnd")
                  return
                }
                setState("justSlidin")
              }}
              breakpoints={{
                768: {
                  slidesPerView: 3,
                  spaceBetween: 48,
                },
              }}
              pagination={true}
              slidesPerView={1}
              spaceBetween={17}>
              {slides}
            </Swiper>
            {/* Add Pagination */}
            <div
              style={{ marginTop: -50, zIndex: 9, position: "relative" }}
              className="d-flex swiper-controls-group">
              <div className="swiper-pagination" />
              <div
                style={{ display: "flex" }}
                className="swiper-controls button-group">
                <div
                  onClick={() => swiperInstance.slidePrev()}
                  className={`${
                    state != "isBeginning" ? "active" : null
                  } slider-btn swiper-button-prev`}>
                  <svg
                    width={8}
                    height={14}
                    viewBox="0 0 8 14"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M0.287829 6.30465L6.3223 0.287096C6.70617 -0.095891 7.32854 -0.095891 7.71222 0.287096C8.09593 0.669742 8.09593 1.29036 7.71222 1.67297L2.37264 6.99759L7.71206 12.322C8.09577 12.7048 8.09577 13.3254 7.71206 13.708C7.32835 14.0908 6.70601 14.0908 6.32215 13.708L0.287674 7.69037C0.095819 7.49895 0 7.24835 0 6.99762C0 6.74676 0.096005 6.49597 0.287829 6.30465Z"
                      fill="#9DAFBD"
                    />
                  </svg>
                </div>
                <div
                  onClick={() => swiperInstance.slideNext()}
                  className={`${
                    state != "isEnd" ? "active" : null
                  } slider-btn swiper-button-next`}>
                  <svg
                    width={8}
                    height={14}
                    viewBox="0 0 8 14"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M7.71217 7.69047L1.6777 13.708C1.29383 14.091 0.671461 14.091 0.287783 13.708C-0.0959275 13.3254 -0.0959275 12.7048 0.287783 12.3221L5.62736 6.99753L0.287938 1.6731C-0.0957722 1.2903 -0.0957722 0.669747 0.287938 0.287101C0.671648 -0.0957002 1.29399 -0.0957002 1.67785 0.287101L7.71233 6.30475C7.90418 6.49616 8 6.74677 8 6.9975C8 7.24835 7.904 7.49915 7.71217 7.69047Z"
                      fill="white"
                    />
                  </svg>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Tools
