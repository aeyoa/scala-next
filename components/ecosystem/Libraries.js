import content from "../../content/Ecosystem/05 Libraries.md"
import matter from "gray-matter"
import LibCard from "./LibCard"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
SwiperCore.use([Pagination])

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

const items = data.libraries.map((el, i) => (
  <SwiperSlide key={i}>
    <LibCard {...el} />
  </SwiperSlide>
))
const alsoItems = data.libraries.map((el, i) => (
  <div key={i} className="col-lg-4 col-md-4">
    <LibCard {...el} />
  </div>
))

const Libraries = () => {
  return (
    <div className="container">
      <div
        className="library  parallax-animate"
        data-class=".library"
        data-start={0}
        data-end={-15}
        data-scroll=".library-clients">
        <div className="library-head" dangerouslySetInnerHTML={html}></div>
        <div
          className="swiper-container library-slider"
          data-options="library-slider">
          <Swiper
            pagination={true}
            slidesPerView={1}
            spaceBetween={30}
            watchOverflow={true}>
            {items}
          </Swiper>
        </div>
        <div className="d-flex swiper-controls-group">
          <div className="swiper-pagination" />
        </div>
        <div className="row">{alsoItems}</div>
      </div>
    </div>
  )
}

export default Libraries
