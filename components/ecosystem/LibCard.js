const LibCard = ({ title, desc, url }) => {
  return (
    <>
      <div className="library-card">
        <div className="git-img">
          <img src="images/Github_logo.png" />
        </div>
        <h3 className="library-card-name">{title}</h3>
        <p className="library-card-text">{desc}</p>
      </div>
      <a href={url} className="github-link">
        View on Github<span>&gt;</span>
      </a>
    </>
  )
}

export default LibCard
