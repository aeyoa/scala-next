import content from "../../content/Ecosystem/04 Facts.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
SwiperCore.use([Pagination])

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

const Box = ({ title, desc }) => (
  <div style={{ marginBottom: 50 }} className="box">
    <h3 className="active-project">{title}</h3>
    <p className="box-text">{desc}</p>
    <div className="daily-circle" />
  </div>
)

const slides = data.items.map((el, i) => (
  <SwiperSlide key={i}>
    <Box {...el} />
  </SwiperSlide>
))

const boxes = data.items.map((el, i) => (
  <div key={i} className="col-lg-3 col-md-3">
    <Box {...el} />
  </div>
))

const Facts = () => {
  return (
    <div
      className="about-scala  parallax-animate"
      data-class=".about-scala"
      data-start={0}
      data-end={-15}
      data-scroll=".about-scala">
      <div className="row">
        <div className="col-lg-6 col-md-6 left-div">
          <h2 style={{ marginTop: 0 }} className="about-scala-head">
            {data.heading}
          </h2>
          <p className="about-scala-text">{data.subtitle}</p>
        </div>
        <div className="col-lg-6 col-md-6 right-div">
          <div className="history">
            <div dangerouslySetInnerHTML={html}></div>
            <div className="call-to-action-btn">
              <a href={data.left_link} className="button-3">
                {data.left_button}
              </a>
              <a href={data.right_link} className="button-3 active">
                {data.right_button}
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="projects">
        <div
          className="swiper-container projects-slider"
          data-options="projects-slider">
          <Swiper pagination={true} slidesPerView={2} spaceBetween={17}>
            {slides}
          </Swiper>
        </div>
        <div className="row project-cont">{boxes}</div>
      </div>
    </div>
  )
}

export default Facts
