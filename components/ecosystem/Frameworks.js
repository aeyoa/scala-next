import content from "../../content/Ecosystem/02 Frameworks.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
import { useState } from "react"
SwiperCore.use([Pagination])

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

let swiperInstance

const slides = data.frameworks.map((el, i) => (
  <SwiperSlide key={i}>
    <div className="container">
      <div className="row">
        <div className="framework-left col-lg-6 col-md-6">
          <div className="ziverge">
            <img src={el.image} className="ziverge-img" />
            <p>{el.desc}</p>
          </div>
          <div className="big-fast-data">
            <h5>{el.subtitle}</h5>
            <div
              className="d-flex"
              dangerouslySetInnerHTML={{
                __html: md.render(el.details),
              }}></div>
          </div>
        </div>
        <div className="col-lg-6 col-md-6 browser-img">
          <img src={el.illustration} className="browser-img" />
          <div className="element-bg framework-slider-bg" />
        </div>
      </div>
      <div className="points z11">
        <div className="row">
          <div className="col-sm-4">
            <div className="text-area text-left">
              <h4>{el.section1_title}</h4>
              <p>{el.section1_desc}</p>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="text-area text-left">
              <h4>{el.section2_title}</h4>
              <p>{el.section2_desc}</p>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="text-area text-left">
              <h4>{el.section3_title}</h4>
              <p>{el.section3_desc}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </SwiperSlide>
))

const Frameworks = () => {
  const [state, setState] = useState("isBeginning")
  return (
    <div
      className="framework parallax-animate"
      data-class=".framework"
      data-start={0}
      data-end={-15}
      data-scroll=".framework">
      <div className="container">
        <div className="row">
          <div
            dangerouslySetInnerHTML={html}
            className="col-sm-6 top-right-heading framework-mardown">
            {/* <h1 className="framework-heading">Frameworks</h1>
            <p className="framework-text">
              Graphic design is the process of visual communication
            </p> */}
          </div>
        </div>
      </div>
      <div
        className="swiper-container pointsSlider"
        data-options="pointsSlider">
        <Swiper
          onSwiper={swiper => (swiperInstance = swiper)}
          pagination
          onSlideChange={() => {
            if (swiperInstance.isBeginning) {
              setState("isBeginning")
              return
            }
            if (swiperInstance.isEnd) {
              setState("isEnd")
              return
            }
            setState("justSlidin")
          }}>
          {slides}
        </Swiper>
        <div className="container">
          <div className="d-flex swiper-controls-group">
            <div className="swiper-pagination" />
            <div className="swiper-controls button-group">
              <div
                onClick={() => swiperInstance.slidePrev()}
                className={`${
                  state != "isBeginning" ? "active" : null
                } slider-btn swiper-button-prev`}>
                <svg
                  width={8}
                  height={14}
                  viewBox="0 0 8 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M0.287829 6.30465L6.3223 0.287096C6.70617 -0.095891 7.32854 -0.095891 7.71222 0.287096C8.09593 0.669742 8.09593 1.29036 7.71222 1.67297L2.37264 6.99759L7.71206 12.322C8.09577 12.7048 8.09577 13.3254 7.71206 13.708C7.32835 14.0908 6.70601 14.0908 6.32215 13.708L0.287674 7.69037C0.095819 7.49895 0 7.24835 0 6.99762C0 6.74676 0.096005 6.49597 0.287829 6.30465Z"
                    fill="#9DAFBD"
                  />
                </svg>
              </div>
              <div
                onClick={() => swiperInstance.slideNext()}
                className={`${
                  state != "isEnd" ? "active" : null
                } slider-btn swiper-button-next`}>
                <svg
                  width={8}
                  height={14}
                  viewBox="0 0 8 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M7.71217 7.69047L1.6777 13.708C1.29383 14.091 0.671461 14.091 0.287783 13.708C-0.0959275 13.3254 -0.0959275 12.7048 0.287783 12.3221L5.62736 6.99753L0.287938 1.6731C-0.0957722 1.2903 -0.0957722 0.669747 0.287938 0.287101C0.671648 -0.0957002 1.29399 -0.0957002 1.67785 0.287101L7.71233 6.30475C7.90418 6.49616 8 6.74677 8 6.9975C8 7.24835 7.904 7.49915 7.71217 7.69047Z"
                    fill="white"
                  />
                </svg>
              </div>
            </div>
          </div>
        </div>
        <div className="call-to-action-btn">
          <a className="button-3">Donwload Now</a>
          <a className="button-3 active">Try Online</a>
        </div>
      </div>
    </div>
  )
}

export default Frameworks
