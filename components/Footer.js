import content from "../content/Header.md"
import matter from "gray-matter"
import { useRouter } from "next/router"

const data = matter(content).data

const Footer = ({ showLogos, hideBanner }) => {
  const router = useRouter()
  const linkItems = data.links.map((elem, index) => {
    return (
      <li key={index}>
        <a
          href={elem.url}
          className={elem.url == router.pathname ? "active" : null}>
          {elem.name}
        </a>
      </li>
    )
  })
  return (
    <footer>
      <div
        className="banner"
        style={
          hideBanner
            ? {
                paddingBottom: 0,
              }
            : {}
        }>
        {showLogos && (
          <div className="container-lg">
            <div className="row">
              <div className="col-md-3">
                <img className="ziverge" src="images/image 1.png" />
              </div>
              <div className="col-md-3 text-center">
                <img className="scalacenter" src="images/image4.png" />
              </div>
              <div className="col-md-3 text-center">
                <img className="irtus" src="images/virtuslab.png" />
              </div>
              <div className="col-md-3 text-center">
                <img className="epfl" src="images/image2.png" />
              </div>
            </div>
          </div>
        )}
      </div>
      {/*<div class="element-bg footer-sky"><img src="images/Bottom-BG-GROUP_2.jpg"/></div>*/}
      <div className="container-lg">
        <div className="logo-area">
          <div className="footer-logo">
            <img src="images/main-logo.png" />
          </div>
          {/* nav footer */}
          <nav>
            <ul>
              <li>
                <a
                  href={"/"}
                  className={"/" == router.pathname ? "active" : null}>
                  Home
                </a>
              </li>
              {linkItems}
            </ul>
          </nav>
          {/* nav footer */}
        </div>
        {/* info footer */}
        <div className="footer-cont">
          <div className="row">
            <div className="col-lg-5 col-md-12 footer-contact">
              <h5 className="footer-cont__title">CONTACT US</h5>
              <div>
                <p>
                  <a className="email" href={`mailto:${data.email}`}>
                    {data.email}
                  </a>
                </p>
                <a className="tel" href={`tel:${data.phone.split(" ").join()}`}>
                  {data.phone}
                </a>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 footer-contact">
              <h5 className="footer-cont__title">LOCATION</h5>
              <p className="footer-cont__text">1st Ave #20831,</p>
              <p className="footer-cont__text">New York, NY 10128</p>
              <p className="footer-cont__text">United States</p>
            </div>
            <div className="col-lg-4 col-md-9 responsive-left">
              <h5 className="footer-cont__title text-right">
                Master Scala 3 for free
              </h5>
              <div className="button-group justify-content-end">
                <a href="/download" className="button-3">
                  Download Now
                </a>
                <a href="/learn" className="button-3 active">
                  {" "}
                  Try online
                </a>
              </div>
            </div>
          </div>
        </div>
        {/* info footer */}
        {/* Copyright */}
        <div className="copyright">
          <p>
            {" "}
            Copyright © 2021 Ziverge Inc.
            <br />
            All rights reserved.
          </p>
          <p>
            Scala is a trademark of EPFL Site designed by{" "}
            <a href="https://xendro.io/" target="_blank">
              Xendro
            </a>
          </p>
        </div>
        {/* Copyright */}
      </div>
    </footer>
  )
}

export default Footer
