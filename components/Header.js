/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../lib/breakpoints"

import content from "../content/Header.md"
import matter from "gray-matter"
import { useRouter } from "next/router"
import { createRef, useEffect, useRef, useState } from "react"

const initial = css(
  mq({
    transition: "opacity 300ms ease, transform 300ms ease",
    opacity: 1,
    transform: "translateY(0px)",
    backgroundColor: "rgba(14, 15, 30, 0)",
  })
)

const shown = css(
  mq({
    transition: "opacity 300ms ease, transform 300ms ease",
    opacity: 1,
    transform: ["translateY(-22px)"],
    backgroundColor: "rgba(14, 15, 30, 0.7)",
  })
)

const hidden = css(
  mq({
    transition: "opacity 300ms ease, transform 300ms ease",
    opacity: 0,
    transform: "translateY(-150%)",
    backgroundColor: "rgba(14, 15, 30, 0)",
  })
)

const styles = { initial, hidden, shown }

function Header({ state }) {
  const router = useRouter()
  const [sidebar, setSidebar] = useState(false)
  const LinkItems = matter(content).data.links.map((elem, index) => {
    return (
      <li key={index}>
        <a
          className={elem.url == router.pathname ? "active" : null}
          href={elem.url}>
          {elem.name}
        </a>
      </li>
    )
  })
  return (
    <header css={styles[state]} className="header-section">
      <div className="container-fluid">
        <div className="header-wrapper">
          <a
            onClick={() => {
              setSidebar(true)
            }}
            className="burger-menu">
            {" "}
            <img src="images/burger-menu.svg" />{" "}
          </a>
          <div className="logo">
            <a href="/">
              <img className="d-show" src="images/logo-main.svg" />
              <img className="d-hide logo-mobile" src="images/logo-mob.svg" />
            </a>
          </div>
          <div
            onClick={() => {
              setSidebar(false)
            }}
            className={`menu-responsive-overlay ${sidebar ? "active" : ""}`}
          />
          <ul className={`menu menu-responsive ${sidebar ? "active" : ""}`}>
            {LinkItems}

            <li>
              <a href="/learn" className="buy-btn m-0 header-button button-1">
                Learn Now
              </a>
            </li>
          </ul>
          <a
            href="/learn"
            className="buy-btn buy-btn-mobile m-0 header-button button-1">
            Learn Now
          </a>
        </div>
      </div>
    </header>
  )
}
export default Header
