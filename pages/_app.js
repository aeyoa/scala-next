import "../styles/main.scss"
import "swiper/swiper.scss"
import "swiper/components/pagination/pagination.min.css"
import Head from "next/head"
import { useRouter } from "next/router"

import Header from "../components/Header"
import ScrollToTop from "../components/ScrollToTop"

import { useEffect, useRef, createRef, useState } from "react"
import { gsap, Power0 } from "gsap"
import parallaxConfig from "../lib/animation"

const titles = {
  "/": "Homepage | Learn Scala 3",
  "/about": "About | Learn Scala 3",
  "/certification": "Certification | Learn Scala 3",
  "/community": "Community | Learn Scala 3",
  "/download": "Download | Learn Scala 3",
  "/ecosystem": "Ecosystem | Learn Scala 3",
  "/jobs": "Jobs | Learn Scala 3",
  "/learn": "Learn | Learn Scala 3",
}

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  const title = titles[router.pathname] || "Learn Scala 3"
  const config = parallaxConfig[router.pathname] || []
  const timerIdRef = useRef()
  const elementsRef = useRef([])
  const isInitialMount = useRef(true)
  const isFirstUpdate = useRef(true)
  const previousScroll = useRef(-1)
  const [realHeaderState, setRealHeaderState] = useState("initial")
  const headerState = useRef("initial")
  const scrollToTopRef = useRef(false)
  const [showScrollToTop, setShowScrollToTop] = useState(false)
  const setHeaderState = state => {
    headerState.current = state
    setRealHeaderState(state)
  }
  const setScrollToTop = state => {
    scrollToTopRef.current = state
    setShowScrollToTop(state)
  }
  const animate = () => {
    window.requestAnimationFrame(() => {
      const scroll = window.pageYOffset || document.documentElement.scrollTop

      // Return, if no scroll happened
      if (scroll == previousScroll.current) return

      // Update ScrollToTop
      const active = scroll > 800
      if (active != scrollToTopRef.current) {
        setScrollToTop(active)
      }

      // Update Header state
      const change = scroll - previousScroll.current
      if (scroll < 65) {
        if (headerState.current != "initial") {
          setHeaderState("initial")
        }
      } else {
        const delta = 2
        if (change > delta) {
          if (headerState.current != "hidden") {
            setHeaderState("hidden")
          }
        }
        if (change < delta * -1) {
          if (headerState.current != "shown") {
            setHeaderState("shown")
          }
        }
      }
      previousScroll.current = scroll

      // Rendering parallax elements
      if (window.innerWidth < 768) return
      for (let i = 0; i < elementsRef.current.length; i++) {
        const current = elementsRef.current[i]
        const el = current.el
        if (!el) continue
        const rect = el.getBoundingClientRect()
        let dif =
          rect.top + (rect.bottom - rect.top) / 2 - window.innerHeight / 2
        if (current.origin == "top") {
          dif = rect.top - window.innerHeight / 2
        }
        const above = rect.bottom < -10
        const below = rect.top > window.innerHeight + 10
        const inView = !(below || above)
        const delta = current.delta || 80
        const ratio = (2 * delta) / window.innerHeight
        if (inView && isFirstUpdate.current) {
          current.offset = -dif * ratio
          continue
        }
        if (current.rotation) {
          gsap.set(el, {
            rotate: scroll / 20 - 90,
            force3D: true,
          })
        } else {
          if (inView) {
            gsap.set(el, {
              y: dif * ratio + current.offset,
              force3D: true,
            })
          }
        }
      }
      isFirstUpdate.current = false
    })
  }
  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false
      elementsRef.current = config.map(el => ({
        el: document.querySelector(el.query),
        delta: el.delta,
        offset: 0,
        rotation: el.rotation,
        origin: el.origin || "center",
      }))
    }
    timerIdRef.current = setInterval(animate, 20)
    return () => {
      clearInterval(timerIdRef.current)
    }
  }, [])
  return (
    <>
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/images/favicon.png"></link>
      </Head>
      <Header state={realHeaderState} />
      <ScrollToTop active={showScrollToTop} />
      <Component {...pageProps} header={realHeaderState} />
    </>
  )
}

export default MyApp
