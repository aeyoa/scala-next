import Head from "next/head"
import Header from "../components/Header"
import Divider from "../components/Divider"
import Footer from "../components/Footer"
import Cover from "../components/homepage/Cover"
import WhyScala from "../components/homepage/WhyScala"
import Points from "../components/homepage/Points"
import PoweredByScala from "../components/PoweredByScala"
import PoweredByScalaContent from "../content/Homepage/04 Powered by Scala.mdx"
import Cases from "../components/homepage/Cases"
import Features from "../components/homepage/Features"
import Explore from "../components/homepage/Explore"
import Thanks from "../components/homepage/Thanks"

export default function Home() {
  return (
    <>
      <main>
        <div className="element-bg home-page-bg">
          <img src="images/Home-header-bg.jpg" />
        </div>
        <section className="top-half-section">
          <Cover />
          <WhyScala />
          <div className="power-point-area">
            <Points />
            <div className="parallax-element text-center center-globe">
              <img src="images/Powered-By.png" />
            </div>
            <PoweredByScala>
              <PoweredByScalaContent />
            </PoweredByScala>
            <div className="parallax-element bottom left net2a net-hide">
              <img src="images/Net 2a.png" />
            </div>
            <div className="parallax-element bottom left net2b net-hide">
              <img src="images/Net 2b.png" />
            </div>
            <div className="parallax-element bottom right net3 net-hide">
              <img src="images/Net 3.png" />
            </div>
            <Divider center />
          </div>
          <div className="parallax-element top right net1 net-hide">
            <img src="images/Net 1.png" />
          </div>
        </section>
        <section className="cases-features-explore">
          <div className="element-bg radial" />
          <div className="element-bg feature-sky">
            <img src="images/Sky Features.png" />
          </div>
          <Cases />
          <Features />
          <Explore />
          <Thanks />
        </section>
        <Footer showLogos />
      </main>
    </>
  )
}
