import Head from "next/head"
import Header from "../components/Header"
import Divider from "../components/Divider"
import Footer from "../components/Footer"
import ScrollToTop from "../components/ScrollToTop"
import Libraries from "../components/ecosystem/Libraries"
import PoweredByScala from "../components/PoweredByScala"
import PoweredByScalaContent from "../content/Ecosystem/06 Logos.mdx"
import Testimonials from "../components/ecosystem/Testimonials"
import Facts from "../components/ecosystem/Facts"
import Tools from "../components/ecosystem/Tools"
import Frameworks from "../components/ecosystem/Frameworks"
import Cover from "../content/Ecosystem/01 Cover.mdx"

export default function Ecosystem() {
  return (
    <main>
      <div className="element-bg ecosystem-page-bg parallax-animate">
        <img
          className="parallax-animate"
          data-class=".ecosystem-page-bg img"
          data-start={-10}
          data-end={50}
          data-scroll=".ecosystem-page"
          src="images/Ecosystem-Sky-Header.jpg"
        />
      </div>

      {/* eco system section */}
      <section className="ecosystem-section">
        <div
          style={{ zIndex: 1 }}
          className="ecosystem-top-bg parallax-element top right parallax-animate echo-p1"
          data-class=".echo-p1"
          data-start={0}
          data-end={20}
          data-scroll="body">
          <img src="images/eco-p1.png" />
        </div>
        <div
          style={{ zIndex: 1 }}
          className="ecosystem-mainscreen-bg parallax-element top left parallax-animate echo-p2"
          data-class=".echo-p2"
          data-start={0}
          data-end={20}
          data-scroll="body">
          <img src="images/eco-p2.png" />
        </div>

        <div
          style={{ zIndex: 2 }}
          className="container-lg eco-content parallax-animate"
          data-class=".eco-content"
          data-start={5}
          data-end={-65}
          data-scroll=".ecosystem-section">
          <div className="mainscreen">
            <div className=" d-flex justify-content-center text-center">
              <div className="mainscreen-cont top-screen-title">
                <Cover />
              </div>
            </div>
          </div>
        </div>
        <div className="framework-tools ">
          <Frameworks />
          <div className="container">
            <Tools />
            <Facts />
          </div>
          <div className="element-bg eco-sky-bg-1">
            <img
              className="parallax-animate"
              data-class=".eco-sky-bg-1 img:first-child"
              data-start={-5}
              data-end={10}
              data-scroll=".eco-sky-bg-1"
              src="images/eco-sky-bg-1.jpg"
            />
            <img
              className="parallax-animate"
              data-class=".eco-sky-bg-1 img:last-child"
              data-start={-5}
              data-end={5}
              data-scroll=".eco-sky-bg-1"
              src="images/eco-sky-bg-2.jpg"
            />
          </div>
        </div>
        <div className="library-clients">
          <div
            className="parallax-element eco-net parallax-animate net-hide"
            data-class=".eco-net"
            data-start={0}
            data-end={-5}
            data-scroll=".library-clients">
            <img src="images/eco-net2.png" />
          </div>
          <Libraries />
          <div className="element-bg urban-video-bg">
            <img src="images/urban-video-bg.png" />
          </div>
          <div className="our-clients">
            <div
              className="element-bg testimonial-bg parallax-animate"
              data-class=".testimonial-bg"
              data-start={-15}
              data-end={0}
              data-scroll=".our-clients">
              <img src="images/eco-sky-testimonial.jpg" />
            </div>
            <PoweredByScala>
              <PoweredByScalaContent />
            </PoweredByScala>
            <Testimonials />
          </div>
        </div>
      </section>
      <Footer />
    </main>
  )
}
