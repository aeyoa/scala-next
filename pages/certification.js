import Head from "next/head"
import Header from "../components/Header"
import Divider from "../components/Divider"
import Footer from "../components/Footer"
import ScrollToTop from "../components/ScrollToTop"
import BackgroundImage from "../components/BackgroundImage"

import content from "../content/Certification/01 Content.md"
import matter from "gray-matter"

const data = matter(content).data

export default function Certification() {
  return (
    <main>
      <div class="element-bg certification-page-bg">
        <img src="images/certifcation-bg.jpg" />
      </div>

      {/* section */}
      <section className="certification-section">
        <div className="container">
          <div>
            <p className="certifiacation_cont__title" data-rellax-speed={5}>
              {data.intro}
            </p>
          </div>
          <div className="text-center">
            <div>
              <img
                style={{ width: 227 }}
                className="label-certifiacation"
                src="images/scala-certificate.png"
              />
            </div>
            <img className="company" src="images/ziv-cert.svg" />
            <p className="certifiacation_cont__text" data-rellax-speed={5}>
              {data.desc}
            </p>
            <a className="button-3 active" href={data.link} target="_blank">
              {data.button}
            </a>
          </div>
        </div>
      </section>
      <Footer />
    </main>
  )
}
