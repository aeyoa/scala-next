import Document, { Html, Head, Main, NextScript } from "next/document"

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    const bodyClasses = {
      "/": "home-page",
      "/about": "about-page",
      "/certification": "certification-page",
      "/community": "community-page",
      "/download": "download-page",
      "/ecosystem": "ecosystem-page",
      "/jobs": "jobs-page",
      "/learn": "learn-page",
    }
    return { ...initialProps, bodyClass: bodyClasses[ctx.pathname] }
  }

  render() {
    return (
      <Html>
        <Head />
        <body className={this.props.bodyClass}>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
